package com.ZoologicoReto.Service;

import com.ZoologicoReto.domain.UbicationType;

public interface IUbicationTypeService {

    public UbicationType create(UbicationType ubicationType);

    public Iterable<UbicationType> read();
}
