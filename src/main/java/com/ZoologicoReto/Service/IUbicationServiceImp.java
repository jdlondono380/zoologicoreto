package com.ZoologicoReto.Service;

import com.ZoologicoReto.Repository.UbicationRepository;
import com.ZoologicoReto.domain.Ubication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IUbicationServiceImp implements IUbicationService {

    @Autowired
    private UbicationRepository ubicationRepository;

    @Override
    public Ubication create(Ubication ubication) {
        return ubicationRepository.save(ubication);
    }

    @Override
    public Iterable<Ubication> read() {
        return ubicationRepository.findAll();
    }
}
