package com.ZoologicoReto.Service;

import com.ZoologicoReto.Repository.dto.AnimalNameGender;
import com.ZoologicoReto.domain.Animales;
import org.springframework.http.ResponseEntity;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface IAnimalesService {

    public ResponseEntity create(Animales animales);

    public Iterable<Animales> read();

    public Animales update(Animales animales);

    public Optional<Animales> getById(String code);

    public Integer quantityAnimals();

    public Iterable<AnimalNameGender> getAnimalNameGender();

    public Integer quantityGender();
}
