package com.ZoologicoReto.Service;

import com.ZoologicoReto.Repository.AnimalesRepository;
import com.ZoologicoReto.Repository.dto.AnimalNameGender;
import com.ZoologicoReto.domain.Animales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IAnimalesServiceImp implements IAnimalesService {

    @Autowired
    private AnimalesRepository animalesRepository;

    @Override
    public ResponseEntity create(Animales animales) {
        if(animalesRepository.findById(animales.getCode()).isPresent()){
            return new ResponseEntity("This code is used", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity(animalesRepository.save(animales), HttpStatus.OK);
        }
    }

    @Override
    public Iterable<Animales> read() {
        return animalesRepository.findAll();
    }

    @Override
    public Animales update(Animales animales) {
        return animalesRepository.save(animales);
    }

    @Override
    public Optional<Animales> getById(String code) {
        return animalesRepository.findById(code);
    }

    @Override
    public Integer quantityAnimals() {
        return animalesRepository.quantityAnimals();
    }

    @Override
    public Iterable<AnimalNameGender> getAnimalNameGender() {
        return animalesRepository.findAnimalNameGender();
    }

    @Override
    public Integer quantityGender() {
        return animalesRepository.quantityGender();
    }
}
