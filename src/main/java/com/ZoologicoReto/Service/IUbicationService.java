package com.ZoologicoReto.Service;

import com.ZoologicoReto.domain.Ubication;

public interface IUbicationService {

    public Ubication create(Ubication ubication);

    public Iterable<Ubication> read();

}
