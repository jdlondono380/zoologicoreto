package com.ZoologicoReto.Service;

import com.ZoologicoReto.Repository.UbicationTypeRepository;
import com.ZoologicoReto.domain.UbicationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IUbicationTypeServiceImp implements IUbicationTypeService {

    @Autowired
    private UbicationTypeRepository ubicationTypeRepository;

    @Override
    public UbicationType create(UbicationType ubicationType) {
        return ubicationTypeRepository.save(ubicationType);
    }

    @Override
    public Iterable<UbicationType> read() {
        return ubicationTypeRepository.findAll();
    }
}
