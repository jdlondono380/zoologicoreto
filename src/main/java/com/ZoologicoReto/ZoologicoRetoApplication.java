package com.ZoologicoReto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZoologicoRetoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZoologicoRetoApplication.class, args);
	}

}
