package com.ZoologicoReto.Repository;

import com.ZoologicoReto.domain.Ubication;
import org.springframework.data.repository.CrudRepository;

public interface UbicationRepository extends CrudRepository<Ubication, Integer> {
}
