package com.ZoologicoReto.Repository;

import com.ZoologicoReto.Repository.dto.AnimalNameGender;
import com.ZoologicoReto.domain.Animales;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AnimalesRepository extends CrudRepository<Animales, String> {

    @Query(value = "select count(animales) from Animales animales")
    Integer quantityAnimals();

    @Query(value = "select animales.name as name, animales.gender as gender from Animales animales")
    Iterable<AnimalNameGender> findAnimalNameGender();

    @Query(value = "select count(animales.gender) from Animales animales")
    Integer quantityGender();
}
