package com.ZoologicoReto.Repository.dto;

public interface AnimalNameGender {

    public String getName();
    public String getGender();
}
