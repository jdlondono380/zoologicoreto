package com.ZoologicoReto.Repository;

import com.ZoologicoReto.domain.UbicationType;
import org.springframework.data.repository.CrudRepository;

public interface UbicationTypeRepository extends CrudRepository<UbicationType, Integer> {
}
