package com.ZoologicoReto.web.res;

import com.ZoologicoReto.Service.IUbicationService;
import com.ZoologicoReto.domain.Ubication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UbicationResource {

    @Autowired
    IUbicationService ubicationService;

    @PostMapping("/ubication")
    public Ubication create(@RequestBody Ubication ubication){
        return ubicationService.create(ubication);
    }

    @GetMapping("/ubication")
    public Iterable<Ubication> read(){
        return ubicationService.read();
    }
}
