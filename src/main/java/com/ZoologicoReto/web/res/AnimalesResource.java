package com.ZoologicoReto.web.res;

import com.ZoologicoReto.Repository.dto.AnimalNameGender;
import com.ZoologicoReto.Service.IAnimalesService;
import com.ZoologicoReto.domain.Animales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class AnimalesResource {

    @Autowired
    IAnimalesService animalesService;

    @PostMapping("/animals")
    public ResponseEntity create(@RequestBody Animales animales){
        return animalesService.create(animales);
    }

    @GetMapping("/animals")
    public Iterable<Animales> read(){
        return animalesService.read();
    }

    @PutMapping("/animals")
    public Animales update(@RequestBody Animales animales){
        return animalesService.update(animales);
    }

    @GetMapping("/animals/count")
    public Integer countAnimals(){
        return animalesService.quantityAnimals();
    }

    @GetMapping("/animals/name-gender")
    public Iterable<AnimalNameGender> getAnimalNameGender(){
        return animalesService.getAnimalNameGender();
    }

    @GetMapping("/animals/count-gender")
    public Integer countGender(){
        return animalesService.quantityGender();
    }
}
