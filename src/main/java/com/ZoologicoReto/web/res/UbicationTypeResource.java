package com.ZoologicoReto.web.res;

import com.ZoologicoReto.Service.IUbicationTypeService;
import com.ZoologicoReto.domain.UbicationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UbicationTypeResource {

    @Autowired
    IUbicationTypeService ubicationTypeService;

    @PostMapping("/ubication-type")
    public UbicationType create(@RequestBody UbicationType ubicationType){
        return ubicationTypeService.create(ubicationType);
    }

    @GetMapping("/ubication-type")
       public Iterable<UbicationType> read(){
        return ubicationTypeService.read();
    }
        ;

}
