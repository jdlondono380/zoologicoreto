package com.ZoologicoReto.domain;

import com.ZoologicoReto.domain.enumeration.Gender;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Animales {

    @Id
    private String code;

    private String name;
    private String race;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ManyToOne
    private Ubication ubication;

}
